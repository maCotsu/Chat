# go to backend directory
cd backend/

# npm ci
npm ci

# install eslint
npm install -g eslint eslint-plugin-import eslint-config-airbnb-base

# lint code
eslint . --ext .js --fix