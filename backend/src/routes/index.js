const router = require('express').Router();
const message = require('./message');
const user = require('./user');
const Main = require('../main');
const auth = require('../auth');
const { validateToken } = require('../jwt');

router.use('/message', validateToken, message);
router.use('/user', validateToken, user);

router.get('/', Main.index);

router.post('/login', auth.login);
router.post('/register', auth.register);

router.get('/login', (req, res) => {
    res.render('login', {
        title: 'SpaceChat - login',
    });
});

module.exports = router;
