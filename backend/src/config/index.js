const production = require('./production');
const development = require('./development');

exports.config = (env) => {
    if (env === 'development') {
        return development;
    }

    return production;
};
