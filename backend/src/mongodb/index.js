const mongodb = require('./mongodb');

module.exports = {
    initialize: mongodb.init,
};
