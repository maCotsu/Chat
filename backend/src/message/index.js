const message = require('./message');
const model = require('./model');

module.exports = {
    getAll: message.getAll,
    getById: message.getById,
    insert: message.insert,
    update: message.update,
    delete: message.delete,
    model,
};
