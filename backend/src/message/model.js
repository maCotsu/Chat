const mongoose = require('mongoose');

const MessageModel = new mongoose.Schema({
    message: { type: String, required: true },
    created_on: { type: Date, required: true },
});

module.exports = mongoose.model('Message', MessageModel);
