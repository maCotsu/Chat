const User = require('./model');

module.exports.getAll = async (req, res) => {
    if (req.decode) {
        await User.find()
            .then((users) => {
                res.status(201).send({ status: 200, result: users });
            })
            .catch((error) => {
                res.status(500).send({ status: 500, error });
            });
    } else {
        res.status(401).send({ status: 401, error: 'Authentication error' });
    }
};

module.exports.getById = async (req, res) => {
    const { id } = req.params;

    if (req.decode) {
        await User.findById(id)
            .then((user) => {
                res.status(201).send({ status: 200, result: user });
            })
            .catch((error) => {
                res.status(500).send({ status: 500, error });
            });
    } else {
        res.status(401).send({ status: 401, error: 'Authentication error' });
    }
};

module.exports.insert = async (req, res) => {
    const { email } = req.body;

    if (req.decode) {
        await User.findOne({ email })
            .then((user) => {
                if (!user) {
                    const newUser = new User(req.body);

                    newUser.save()
                        .then((success) => {
                            res.status(200).send({ status: 200, success });
                        })
                        .catch((error) => {
                            res.status(500).send({ status: 500, error });
                        });
                }
            })
            .catch((error) => {
                res.status(500).send({ status: 500, error });
            });
    } else {
        res.status(401).send({ status: 401, error: 'Authentication error' });
    }
};

module.exports.update = async (req, res) => {
    const { id } = req.params;

    if (req.decode) {
        User.findByIdAndUpdate(id, req.body)
            .then((success) => {
                res.status(201).send({ status: 201, result: success });
            })
            .catch((error) => {
                res.status(500).send({ status: 500, error });
            });
    } else {
        res.status(401).send({ status: 401, error: 'Authentication error' });
    }
};

module.exports.delete = async (req, res) => {
    const { id } = req.params;

    if (req.decode) {
        User.findByIdAndDelete(id)
            .then((success) => {
                res.status(202).send({ status: 202, result: success });
            })
            .catch((error) => {
                res.status(500).send({ status: 500, error });
            });
    } else {
        res.status(401).send({ status: 401, error: 'Authentication error' });
    }
};
