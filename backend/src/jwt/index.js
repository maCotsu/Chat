const jwt = require('jsonwebtoken');

module.exports = {
    validateToken: (req, res, next) => {
        const header = req.headers.authorization;
        let result;

        if (header) {
            const token = req.headers.authorization.split(' ')[1];
            const options = {
                expiresIn: '2d',
            };

            try {
                result = jwt.verify(token, 'secret', options);
                req.decode = result;
                next();
            } catch (error) {
                throw new Error(error);
            }
        } else {
            result = {
                error: 'Authentication error. Token required.',
                status: 401,
            };
            res.status(401).send(result);
        }
    },
};
