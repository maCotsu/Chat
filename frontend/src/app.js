'use strict';

import Utils from './services/Utils';

import Index from './pages/Index';

const routes = {
    '/': Index,
    '/login' : 'login',
    '/register': 'register'
}

const router = () => {
    const app = null || document.querySelector("#app");
    const request = Utils.parseRequestUri();
    const parsedURL = (request.page ? '/' + request.page : '/') + (request.id ? '/:id' : '') + (request.verb ? '/' + request.verb : '')

    let page = routes[parsedURL] ? routes[parsedURL] : Index;

    app.innerHTML = page.render();
};

window.addEventListener('hashchange', router);
window.addEventListener('load', router);