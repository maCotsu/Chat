const Utils = {
    parseRequestUri : () => {
        const url = location.hash.slice(1).toLocaleLowerCase() || '/';
        const r = url.split('/');
        const request = {
            page: null,
            id: null,
            verb: null,
        };

        request.page = r[1];
        request.id = r[2];
        request.verm = r[3];

        return request;
    },
};

export default Utils;
